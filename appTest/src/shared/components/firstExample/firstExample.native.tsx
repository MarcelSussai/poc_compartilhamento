/* eslint-disable prettier/prettier */
import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import styled from 'styled-components/native';

const Container = styled.View`
  flex: 1;
  height: 200px;
  background-color: #aeeeff;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const TextTeste = styled.Text`
  font-size: 24px;
`;

interface params {
  tText: string,
}

const FirstExample = (props: params) => {

  const { tText } = props;

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Container>
          <TextTeste> {tText} </TextTeste>
        </Container>
      </ScrollView>
    </SafeAreaView>
  )
}

export default FirstExample
