
import Head from '../src/components/head'
import FirstExample from '../src/shared/components/firstExample/firstExample';
import { Main } from '../src/styles/Main_Styled'


const Home = () => {
  return (
  <>
    <Head title="Home" />
    <Main>
      <FirstExample tText={'Texto de Teste'} />
    </Main>
  </>
);

}

export default Home;