import styled from 'styled-components';

const Container = styled.div`
  width: 100vw;
  height: 200px;
  background-color: #aeeeff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0;
  padding: 8px;
`;

const TextTeste = styled.div`
  font-size: 24px;
`;

interface params {
  tText: string,
}

const FirstExample = (props: params) => {

  const { tText } = props;

  return (
    <>
      <Container>
        <TextTeste> {tText} </TextTeste>
      </Container>
    </>
  )
}

export default FirstExample